#include <GL/glut.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/* Define Glu Objects */
int about_int=0;

GLUquadricObj *Cylinder;
GLUquadricObj *Disk;

struct tm *newtime;
time_t ltime;
int M_TWOPI=0;
GLfloat rx, ry, rz, angle;
/* END Glu objects */


void init(void){
    glClearColor (0.0, 0.0, 0.0, 0.0);
}

void Draw_gear( void )
{

	int i;
	glPushMatrix();
	gluCylinder(Cylinder, 2.5, 2.5, 1, 16, 16);
	gluDisk(Disk, 0, 2.5, 32, 16);
	glTranslatef(0,0,1);
    gluDisk(Disk, 0, 2.5, 32, 16);
	glPopMatrix();
    for( i = 0; i < 8; i++)
		{
	    glPushMatrix();
		glTranslatef( 0.0, 0.0, 0.50);
		glRotatef( (360/8) * i, 0.0, 0.0, 1.0);
		glTranslatef( 3.0, 0.0, 0.0);
		glutSolidCube( 1.0 );
		glPopMatrix();
	    }
}


int main(int argc, char** argv){
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize (500, 500);
    glutInitWindowPosition (50, 50);
    glutCreateWindow (argv[0]);
    glutSetWindowTitle("Analogue Clock - Ashraful");
    init ();
    glutMainLoop();
    return 0;
}
